import React, {Component} from 'react';
import {Button, Card, CardHeader, CardText, CardTitle} from "reactstrap";
import Moment from "react-moment";
import {connect} from "react-redux";
import {deletePost, fetchPost} from "../../../store/actionFullPost";

class FullPost extends Component {

    componentDidMount() {
        this.props.fetchPost(this.props.match.params.id);
    }
    render() {
        return (
            <div>
                <Card body inverse color="info">
                    <CardHeader>
                        <Moment format="YYYY-MM-DD HH:mm">
                            {this.props.post.time}
                        </Moment>
                    </CardHeader>
                    <CardTitle style={{textAlign: 'center'}}>{this.props.post.title}</CardTitle>
                    <CardText>{this.props.post.text}</CardText>
                    <Button color="secondary" onClick={() =>this.props.deletePost(this.props.history)}>Delete</Button>
                </Card>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        post: state.post,
        loading: state.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchPost: (id) => dispatch(fetchPost(id)),
        deletePost: (h)=> dispatch(deletePost(h))
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(FullPost);
