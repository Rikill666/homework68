import React, {Component} from 'react';
import Moment from "react-moment";
import {Card, CardHeader, CardTitle} from "reactstrap";

class Post extends Component {
    render() {
        return (
            <Card body>
                <CardHeader>
                    <Moment format="YYYY-MM-DD HH:mm">
                        {this.props.time}
                    </Moment>
                </CardHeader>
                <CardTitle >{this.props.title}</CardTitle>
                {this.props.children}
            </Card >
        );
    }
}

export default Post;