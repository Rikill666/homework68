import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import Post from "./Post/Post";
import {Button} from "reactstrap";
import {connect} from "react-redux";
import {fetchPosts} from "../../store/actionsPost";
import Spinner from "../../components/UI/Spinner/Spinner";

class Posts extends Component {
    componentDidMount() {
        this.props.fetchPosts();
    }

    render() {
        return (
            this.props.posts ?
                <div>
                    {Object.keys(this.props.posts).map(id => (
                        <Post key={id} time={this.props.posts[id].time} title={this.props.posts[id].title}>
                            <Button tag={NavLink} to={'/posts/' + id} exact className="w-25"> >Read more</Button>
                        </Post>
                    ))}
                </div> : <Spinner/>
        );
    }
}
const mapStateToProps = state => {
    return {
        posts: state.posts,
        loading: state.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchPosts: () => dispatch(fetchPosts())
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Posts);