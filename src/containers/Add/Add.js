import React, {Component} from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {savePost} from "../../store/actionsAddPost";
import {bindActionCreators} from "redux";
import * as actionsAddPost from "../../store/actionsAddPost";
class Add extends Component {

    render() {
        return (
            <div>
                <Form onSubmit={this.props.savePost}>
                    <FormGroup>
                        <Label for="exampleEmail">Add title</Label>
                        <Input type="text"
                               name="title"
                               id="exampleEmail"
                               onChange={e=>this.props.onChangeValue.onChangeValue(e.target.value, e.target.name)}
                               value={this.props.title}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="exampleText">Add text</Label>
                        <Input type="textarea"
                               name="text"
                               id="exampleText"
                               onChange={e=>this.props.onChangeValue.onChangeValue(e.target.value, e.target.name)}
                               value={this.props.text}
                        />
                    </FormGroup>
                    <Button>Save</Button>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        title: state.title,
        text: state.text
    };
};

const mapDispatchToProps = dispatch => {
    return {
        savePost : () => dispatch(savePost()),
        onChangeValue: bindActionCreators(actionsAddPost, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Add);