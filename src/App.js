import React from 'react';
import {Switch, Route} from "react-router-dom";
import Counter from "./containers/Counter/Counter";
import Posts from "./containers/Posts/Posts";
import Add from "./containers/Add/Add";
import FullPost from "./containers/Posts/FullPost/FullPost";
import {Container} from "reactstrap";
import NavMenu from "./components/NavMenu/NavMenu";

const App = () => {
    return (
        <Container>
            <NavMenu/>
            <Switch>
                <Route path="/" exact component={Counter}/>
                <Route path="/add" exact component={Add}/>
                <Route path="/posts" exact component={Posts}/>
                <Route path="/posts/:id" component={FullPost}/>
                <Route render={() => <h1>Not Found</h1>}/>
            </Switch>
        </Container>
    );
};

export default App;
