import axiosOrder from "../axiosOrders";
export const ON_CHANGE_INFO = "ON_CHANGE_INFO";
export const SAVE_POST_SUCCESS = 'SAVE_POST_SUCCESS';
export const SAVE_POST_ERROR = 'SAVE_POST_ERROR';

export function onChangeValue(value, name) {
    return {
        type: ON_CHANGE_INFO,
        fieldName: name,
        value: value
    };
}
export const savePostSuccess = (post) =>{
    return {type: SAVE_POST_SUCCESS, post};
};
export const savePostError = () =>{
    return {type: SAVE_POST_ERROR};
};

export const savePost = () => {
    return async (dispatch, getState) => {
        try{
            const post = {
                title: getState().title,
                text: getState().text,
                time: new Date()
            };
            const response = await axiosOrder.post('/posts.json', post);
            dispatch(savePostSuccess(response.data));
        }
        catch (e) {
            dispatch(savePostError());
        }
    };
};

