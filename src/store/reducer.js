import {
    ADD,
    DECREMENT,
    FETCH_COUNTER_REQUEST,
    FETCH_COUNTER_SUCCESS,
    INCREMENT,
    SAVE_COUNTER_SUCCESS,
    SUBTRACT
} from "./actionsCounter";
import {FETCH_POSTS_REQUEST, FETCH_POSTS_SUCCESS} from "./actionsPost";
import {ON_CHANGE_INFO, SAVE_POST_SUCCESS} from "./actionsAddPost";
import {DELETE_POST_SUCCESS, FETCH_POST_REQUEST, FETCH_POST_SUCCESS} from "./actionFullPost";


const initialState = {
    counter: 0,
    loading: false,
    posts:{},
    title:"",
    text:"",
    post: {}
};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case INCREMENT:
            return {...state, counter: state.counter + 1};
        case DECREMENT:
            return {...state, counter: state.counter - 1};
        case ADD:
            return {...state, counter: state.counter + action.amount};
        case SUBTRACT:
            return {...state, counter: state.counter - action.amount};
        case FETCH_COUNTER_REQUEST:
            return {...state, loading: true};
        case FETCH_COUNTER_SUCCESS:
            return {...state, counter: action.counter, loading: false};
        case SAVE_COUNTER_SUCCESS:
            return {...state, counter: action.counter, loading: false};
        case FETCH_POSTS_REQUEST:
            return {...state, loading: true};
        case FETCH_POSTS_SUCCESS:
            return {...state, posts: action.posts, loading: false};
        case FETCH_POST_REQUEST:
            return {...state, loading: true};
        case FETCH_POST_SUCCESS:
            return {...state, post: action.post, loading: false};
        case DELETE_POST_SUCCESS:
            return {...state, post: {}, loading: false};
        case SAVE_POST_SUCCESS:
            return {...state, posts: action.posts, loading: false};
        case ON_CHANGE_INFO:
            return {...state, [action.fieldName]: action.value};
        default:
            return state;
    }
};
export default reducer;