import axiosOrder from "../axiosOrders";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_ERROR = 'FETCH_POSTS_ERROR';


export const fetchPostsRequest = () => {
    return {type: FETCH_POSTS_REQUEST};
};

export const fetchPostsSuccess = (posts) => {
    return {type: FETCH_POSTS_SUCCESS, posts};
};

export const fetchPostsError = () => {
    return {type: FETCH_POSTS_ERROR};
};

export const fetchPosts = () => {
    return dispatch => {
        dispatch(fetchPostsRequest());
        axiosOrder.get('/posts.json').then(response => {
            dispatch(fetchPostsSuccess(response.data));
        }, error => {
            dispatch(fetchPostsError());
        });
    }
};


