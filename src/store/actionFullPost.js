import axiosOrder from "../axiosOrders";

export const FETCH_POST_REQUEST = 'FETCH_POST_REQUEST';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_ERROR = 'FETCH_POST_ERROR';

export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';
export const DELETE_POST_ERROR = 'DELETE_POST_ERROR';

export const deletePostSuccess = () => {
    return {type: DELETE_POST_SUCCESS};
};

export const deletePostError = () => {
    return {type: DELETE_POST_ERROR};
};
export const fetchPostRequest = () => {
    return {type: FETCH_POST_REQUEST};
};

export const fetchPostSuccess = (post) => {
    return {type: FETCH_POST_SUCCESS, post};
};

export const fetchPostError = () => {
    return {type: FETCH_POST_ERROR};
};

export const deletePost = (history) => {
    return (dispatch, getState) => {
        const post = getState().post;
        axiosOrder.delete(`/posts/${post.id}.json`).then(response => {
            dispatch(deletePostSuccess());
            history.push("/");
        }, error => {
            dispatch(deletePostError());
        });
    }
};
export const fetchPost = (id) => {
    return dispatch => {
        dispatch(fetchPostRequest());
        axiosOrder.get(`/posts/${id}.json`).then(response => {
            dispatch(fetchPostSuccess({...response.data, id: id}))
        }, error => {
            dispatch(fetchPostError());
        });
    }
};
